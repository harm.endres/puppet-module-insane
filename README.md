Please look at https://gitlab.com/hendr.es because of the subgroup problem

https://gitlab.com/gitlab-org/gitlab-ce/issues/39485

# insane - the insane puppet helper class

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with insane](#setup)
    * [What insane affects](#what-insane-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with insane](#beginning-with-insane)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

my bored interim...

it was actually just to troll my colleagues, but it works.

## Setup

```
mod 'insane',
  :git => 'https://gitlab.com/harm.endres/puppet-module-insane.git',
  :tag => '0.0.2'
```

### What insane affects **OPTIONAL**

everything you add to hiera

### Setup Requirements **OPTIONAL**

stdlib

### Beginning with insane

```
include insane
```

## Usage

hiera example

```yaml
---
classes:
  - insane
insane::data:
  package:
    nginx:
      attributes:
        ensure: latest
    zsh:
      attributes: {}
  file:
    /etc/nginx/nginx_example.conf:
      template: insane/template.erb
      attributes:
        mode: '0777'
        notify: "Service[nginx]"
    /etc/mime.types:
      attributes:
        content: application/x-texinfo  texi texinfo
  service:
    nginx:
      attributes:
        ensure: running
        require: "Package[nginx]"
    ntpd:
      attributes: {}
  user:
    bar:
      attributes:
        home: /home/baz
        shell: /usr/bin/zsh
    foo:
      attributes:
        shell: /usr/bin/zsh
  cron:
    name:
      attributes:
        command: asdsadas
        hour: 3
    name2:
      attributes:
        hour: 0
        command: othercommand
  # you can define any class or resource type ...
  class:
    apache:
      attributes:
        default_vhost: false
    apache::vhosts:
      attributes:
        vhosts:
          vhost1:
            port: 80
            add_listen: false
            servername: "%{::fqdn}"
            docroot: /var/www
  apache::vhost:
    vhost2:
      attributes:
        port: 80
        add_listen: false
        servername: "%{::fqdn}"
        docroot: /var/www
  apache::listen:
    '80': {}

insane::defaults:
  file:
    ensure: file
    owner: root
    group: root
    mode: '0640'
  service:
    ensure: stopped
  cron:
    ensure: present
    user: root
    minute: '0'
  package:
    ensure: present
  user:
    require: "Package[zsh]"
```

## Reference

https://puppet.com/docs/puppet/5.3/type.html

## Limitations

Puppet >= 4.x

## Development

Feel free to open merge requests/issues!

## Release Notes/Contributors/Etc. **Optional**

ToDo
- spec tests
- all the things
