# Class: insane
# ===========================
#
# Full description of class insane here. uhm insane?
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `data`
# holds the data for all resource types
# * `defaults`
# holds the defaults for all resource types
#
# Examples
# --------
#
# @example
#    class { 'insane':
#      data => {
#        file => {
#          /etc/ntp.conf => {
#              attributes => {
#                ensure => 'file',
#                            }
#          }
#        },
#      },
#      defaults => {
#        file => {
#          ensure: 'directory',
#          mode: '0644',
#        }
#      }
#    }
#
#
# Authors
# -------
#
# Harm Endres <harm.mueller@gmail.com>
#
# Copyright
# ---------
#
# Copyright 2017 Harm Endres, unless otherwise noted.
#
class insane (
  Hash $data,
  Hash $defaults,
){
  $data.each  |String $resource_type, $resource_data| {
    if $defaults.has_key($resource_type) {
      $_defaults = $defaults[$resource_type]
    } else {
      $_defaults = {}
    }
    $resource_data.each |String $item_name,Hash $item_data| {
      if $item_data.has_key(attributes) {
        $_attributes = $item_data['attributes']
      } else {
        $_attributes = {}
      }
      # check if template function should be called
      if $resource_type == 'file' and $item_data.has_key('template') {
        Resource[$resource_type] {
          default:
            *       => $_defaults;
          $item_name:
            *       => $_attributes,
            content => template($item_data['template']),
        }
      } elsif $resource_type == 'file' and $item_data.has_key('inline_template') {
        Resource[$resource_type] {
          default:
            *       => $_defaults;
          $item_name:
            *       => $_attributes,
            content => inline_template($item_data['inline_template']),
          }
      } else {
        Resource[$resource_type] {
          default:
            * => $_defaults;
          $item_name:
            * => $_attributes,
        }
      }
    }
  }
}
